import { Component } from '@angular/core';
import {FirebaseUISignInSuccessWithAuthResult, FirebaseUISignInFailure} from 'firebaseui-angular';
import { AngularFireMessaging } from '@angular/fire/messaging';
import { AngularFireDatabase } from '@angular/fire/database';
import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';
import { mergeMapTo } from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'hosting';
    constructor(private push: AngularFireMessaging, private auth: AngularFireAuth
      , private store: AngularFirestore) {}

    successCallback(signInSuccessData: FirebaseUISignInSuccessWithAuthResult) {
      console.log('GREAT', signInSuccessData.authResult.additionalUserInfo.username);
      console.log(this.auth.auth.currentUser.uid);
      this.permission(this.auth.auth.currentUser.uid);
    }

    permission(uid: string) {
      this.push.requestPermission
      .pipe(mergeMapTo(this.push.tokenChanges))
      .subscribe(
        (token) => {
          console.log(token);
          this.store.collection<any>('fcm_tokens').doc(uid).set({
            token: token
          });
        },
        (error) => { console.error(error); }
      );
    }
}
